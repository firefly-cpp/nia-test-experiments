#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='nia-experiment',
      version='1.0',

      packages=find_packages(),

      scripts=["src/niapy_custom_problem.py", "src/niapy_pso.py", "run.sh"],
     )
 
