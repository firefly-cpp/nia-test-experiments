#!/bin/bash

magenta='\033[0;35m'
clear='\033[0m'

echo -e "Running ${magenta}nature-inspired algorithms${clear}"
for f in *.py; do python "$f" | grep Traceback ; done
echo "Done"

